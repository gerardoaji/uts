/**
 * Initiate the app at the beginning
 */


/**
 * Generating unique ID for new Input
*/
function guid() {
    return parseInt(Date.now() + Math.random());
}

/**
 * Create and Store New Member
 */
function saveMahasiswaInfo() {
    var keys = ['nim', 'nama', 'jenis_kelamin', 'agama'];
    var obj = {};

    keys.forEach(function (item, index) {
        var result = document.getElementById(item).value;
        if (result) {
            obj[item] = result;
        }
    })

    var mahasiswa = getMahasiswa();

    if (!mahasiswa.length) {
        $('.show-table-info').addClass('hide');
    }

    if (Object.keys(obj).length) {
        var mahasiswa = getMahasiswa();
        obj.id = guid();
        mahasiswa.push(obj);
        var data = JSON.stringify(mahasiswa);
        localStorage.setItem("mahasiswa", data);
        clearFields();
        insertIntoTableView(obj, getTotalRowOfTable());
        $('#addnewModal').modal('hide')
    }
}

/**
 * Clear Create New Member Form Data0
 */
function clearFields() {
    $('#input_form')[0].reset();
}

/** 
 * Get All Members already stored into the local storage
*/
function getMahasiswa() {
    var mahasiswaRecord = localStorage.getItem("mahasiswa");
    var mahasiswa = [];
    if (!mahasiswaRecord) {
        return mahasiswa;
    } else {
        mahasiswa = JSON.parse(mahasiswaRecord);
        return mahasiswa;
    }
}

/**
 * Format Age of All Members
 */
function getFormattedMahasiswa() {
    var mahasiswa = getMahasiswa();



    return mahasiswa;

}


/**
 * Calculate Age in current date from birthdate 
 * 
 * @param {string} date 
 
function calculateAge(date) {
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
*/


/**
 * Populating Table with stored data
 */
function getTableData() {
    $("#mahasiswa_table").find("tr:not(:first)").remove();

    var searchKeyword = $('#cari_nim').val();
    var mahasiswa = getFormattedMahasiswa();

    var filteredMahasiswa = mahasiswa.filter(function (item, index) {
        return item.nim.toLowerCase().includes(searchKeyword.toLowerCase()) ||
            item.nama.toLowerCase().includes(searchKeyword.toLowerCase()) 
    });

    if (!filteredMahasiswa.length) {
        $('.show-table-info').removeClass('hide');
    } else {
        $('.show-table-info').addClass('hide');
    }

    filteredMahasiswa.forEach(function (item, index) {
        insertIntoTableView(item, index + 1);
    })
}

/**
 * Inserting data into the table of the view
 * 
 * @param {object} item 
 * @param {int} tableIndex 
 */
function insertIntoTableView(item, tableIndex) {
    var table = document.getElementById('mahasiswa_table');
    var row = table.insertRow();
    var idCell = row.insertCell(0);
    var nimCell = row.insertCell(1);
    var namaCell = row.insertCell(2);
    var jenisKelaminCell = row.insertCell(3);
    var agamaCell = row.insertCell(4);
    var actionCell = row.insertCell(5);
    
    idCell.innerHTML = tableIndex;
    nimCell.innerHTML = item.nim;
    namaCell.innerHTML = item.nama;
    jenisKelaminCell.innerHTML = item.jenis_kelamin;
    agamaCell.innerHTML = item.agama;
    var guid = item.id;

    actionCell.innerHTML = '<button class="btn btn-sm btn-default" onclick="showMahasiswaData(' + guid + ')">View</button> ' +
    '<button class="btn btn-sm btn-primary" onclick="showEditModal(' + guid + ')">Edit</button> ' +
    '<button class="btn btn-sm btn-danger" onclick="showDeleteModal(' + guid + ')">Delete</button>';
}


/**
 * Get Total Row of Table
 */
function getTotalRowOfTable() {
    var table = document.getElementById('mahasiswa_table');
    return table.rows.length;
}

/**
 * Show Single Member Data into the modal
 * 
 * @param {string} id 
 */
function showMahasiswaData(id) {
    var allMahasiswa = getMahasiswa();
    var mahasiswa = allMahasiswa.find(function (item) {
        return item.id == id;
    })

    $('#show_nim').val(mahasiswa.nim);
    $('#show_nama').val(mahasiswa.nama);
    $('#show_jenis_kelamin').val(mahasiswa.jenis_kelamin);
    $('#show_agama').val(mahasiswa.agama);

    $('#showModal').modal();

}


/**
 * Show Edit Modal of a single member
 * 
 * @param {string} id 
 */
function showEditModal(id) {
    var allMahasiswa = getMahasiswa();
    var mahasiswa = allMahasiswa.find(function (item) {
        return item.id == id;
    })

    $('#edit_nim').val(mahasiswa.nim);
    $('#edit_nama').val(mahasiswa.nama);
    $('#edit_jenis_kelamin').val(mahasiswa.jenis_kelamin);
    $('#edit_agama').val(mahasiswa.agama);
    $('#mahasiswa_id').val(id);

    $('#editModal').modal();
}


/**
 * Store Updated Member Data into the storage
*/
function updateDataMahasiswa() {

    var allMahasiswa = getMahasiswa();
    var mahasiswaId = $('#mahasiswa_id').val();

    var mahasiswa = allMahasiswa.find(function (item) {
        return item.id == mahasiswaId;
    })

    mahasiswa.nim = $('#edit_nim').val();
    mahasiswa.nama = $('#edit_nama').val();
    mahasiswa.jenis_kelamin = $('#edit_kelamin').val();
    mahasiswa.agama = $('#edit_agama').val();

    var data = JSON.stringify(allMahasiswa);
    localStorage.setItem('mahasiswa', data);

    $("#mahasiswa_table").find("tr:not(:first)").remove();
    getTableData();
    $('#editModal').modal('hide')
}

/**
 * Show Delete Confirmation Dialog Modal
 * 
 * @param {int} id 
 */
function showDeleteModal(id) {
    $('#deleted-mahasiswa-id').val(id);
    $('#deleteDialog').modal();
}

/**
 * Delete single member
*/
function deleteMahasiswaData() {
    var id = $('#deleted-mahasiswa-id').val();
    var allMahasiswa = getMahasiswa();

    var storageUsers = JSON.parse(localStorage.getItem('mahasiswa'));

    var newData = [];

    newData = storageUsers.filter(function (item, index) {
        return item.id != id;
    });

    var data = JSON.stringify(newData);

    localStorage.setItem('mahasiswa', data);
    $("#mahasiswa_table").find("tr:not(:first)").remove();
    $('#deleteDialog').modal('hide');
    getTableData();

}

/**
 * Sorting table data through type, e.g: first_name, email, last_name etc.
 * 
 * @param {string} type 
 */
function sortBy(type)
{
    $("#mahasiswa_table").find("tr:not(:first)").remove();

    var totalClickOfType = parseInt(localStorage.getItem(type));
    if(!totalClickOfType) {
        totalClickOfType = 1;
        localStorage.setItem(type, totalClickOfType);
    } else {
        if(totalClickOfType == 1) {
            totalClickOfType = 2;
        } else {
            totalClickOfType = 1;
        }
        localStorage.setItem(type, totalClickOfType);
    }

    var searchKeyword = $('#cari_nim').val();
    var mahasiswa = getFormattedMahasiswa();

    var sortedMahasiswa = mahasiswa.sort(function (a, b) {
        return (totalClickOfType == 2) ? a[type] > b[type] : a[type] < b[type];
    });

    sortedMahasiswa.forEach(function (item, index) {
        insertIntoTableView(item, index + 1);
    })
}